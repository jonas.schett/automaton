﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum State
{
    off,
    one,
    two,
    three,
    dontCare,
    MAX_VALUE
}

[SerializeField]
public struct Neighbours
{
    public Cell N;
    public Cell NE;
    public Cell E;
    public Cell SE;
    public Cell S;
    public Cell SW;
    public Cell W;
    public Cell NW;
}

[SelectionBase]
public class Cell : MonoBehaviour
{
    public State currentState;
    [HideInInspector]
    public State nextState;
    public State desiredState;

    private State stateAtStartOfPause;

    [SerializeField]
    private SpriteRenderer background;
    [SerializeField]
    private SpriteRenderer foreground;
    [SerializeField]
    private SpriteRenderer desiredStateTag;

    private Dictionary<State, Tuple<Sprite, Sprite>> stateToSpriteMap;

    public bool CoverActive { private set; get; }

    public Neighbours neighbours;
    private Cell[] neighboursList;

    private State maxStateForGame;

    public void Initialise(Dictionary<State, Tuple<Sprite, Sprite>> map, State maxStateForGame, bool coverActive, State desiredState = State.dontCare, State state = State.off)
    {
        neighboursList = new Cell[]{ neighbours.N, neighbours.NE, neighbours.E, neighbours.SE,
                                     neighbours.S, neighbours.SW, neighbours.W, neighbours.NW};
        this.maxStateForGame = maxStateForGame;
        stateToSpriteMap = map;

        
        currentState = State.dontCare;
        nextState = state;

        this.desiredState = desiredState;
        desiredStateTag.sprite = stateToSpriteMap[desiredState].Item2;

        CoverActive = coverActive;
        SetCoverActive(coverActive);
        ChangeState(true);
    }
    public void PauseStarted()
    {
        stateAtStartOfPause = currentState;
    }
    public bool CheckDesiredState()
    {
        return desiredState == State.dontCare ? true : desiredState == currentState;
    }
    public int ToggleCheckCoverActive(int tries)
    {
        if(!CoverActive)
        {
            return Toggle(tries);
        }
        return 0;
    }
    public int Toggle(int tries)
    {
        int returnVal = -1;
        nextState = (int)currentState + 1 > (int)maxStateForGame ? State.off : (State)((int)currentState + 1);

        // here we actually change back to the state that was present at the beginning
        // of the pause, so we return 1
        if(stateAtStartOfPause == nextState)
        {
            returnVal = 1;
        }

        if(tries > 0 || returnVal == 1)
        {
            SetState(nextState);
            return returnVal;
        }

        Debug.Log("Out of tries");
        return 0;
        
    }
    public void SetState(State state, bool pauseState = false)
    {
        nextState = state;
        ChangeState(pauseState);
    }
    public void ChangeState(bool pauseStateUpdate = false)
    {
        if(currentState != nextState)
        {
            currentState = nextState;
            foreground.sprite = stateToSpriteMap[currentState].Item1;
        }

        if(pauseStateUpdate)
        {
            stateAtStartOfPause = currentState;
        }
    }
    public void UpdateNextState(BaseRule rule)
    {
        nextState = rule.ApplyRule(neighboursList, currentState);
    }

    public void SetCoverActive(bool coverActive)
    {
        CoverActive = coverActive;
    }
    public void OnMouseDown()
    {
        if(!GridManager.GameIsInterrupted)
        {
            GridManager.Tries += ToggleCheckCoverActive(GridManager.Tries);
        }
    }

#if (UNITY_EDITOR)
    public void SetStateToMaterialMap(Dictionary<State, Tuple<Sprite, Sprite>> map)
    {
        stateToSpriteMap = map;
    }
    public void SetDesiredStateEditor(State state)
    {
        desiredState = state;
    }
#endif
}
