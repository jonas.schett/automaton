﻿using System.Collections;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    public static bool LevelComplete = false;
    public static bool LevelFailed = false;
    public static bool GameIsPaused = true;
    public static bool GameIsInterrupted = false;
    public static int Tries = 0;

    public static int LevelPackID;
    public static int LevelID = 0;

#if(UNITY_EDITOR)
    public LevelPack pack;
#endif

    public static LevelPack levelPack;

    private static float tickTime = 1.0f;

    public InfoItem triesUsedInfo;
    public InfoItem ticksUsedInfo;

    private Level currentLevel;
    private BaseRule rule;

    
    private State maxStateForGame;

    [SerializeField]
    private Grid gridPrefab;
    private Grid grid;

    private int tries;
    private int maxTries;
    private int rounds;
    private int maxRounds;

    private void Awake()
    {
        // initialise my grid here
        grid = Instantiate(gridPrefab);
        grid.transform.SetParent(transform);
    }

    // Start is called before the first frame update
    void Start()
    {

#if (UNITY_EDITOR)
        if(levelPack == null)
        {
            levelPack = pack;
        }
#endif

        // ensure the levelID is correct
        LevelID = LevelID % levelPack.Levels.Length;
        rule = levelPack.Rule;

        // Ensure game is not interrupted at startup,
        // could happen due to going into main menu
        // and back
        InterruptGame(false);
        currentLevel = levelPack.Levels[LevelID];
        Tries = currentLevel.numTries;
        rounds = currentLevel.numRounds;

        Camera.main.orthographicSize = currentLevel.xSize * 1.2f;

        ResetTriesAndTicks();
        maxStateForGame = State.one;

        
        grid.InitialiseCells(maxStateForGame, currentLevel);
        StartCoroutine("Tick");
    }

    

    IEnumerator Tick()
    {
        bool finished = false;
        while (!finished)
        {
            // check is game is paused and only update
            // if game is not paused
            if (!GameIsPaused)
            {
                if(rounds != 0)
                {
                    grid.UpdateCells(rule);
                } 
            }

            // check if game is complete by success
            finished = true;
            bool allDead = grid.CheckAllDead();
            finished = grid.CheckFinished();

            bool levelFails = false;
            if (!GameIsPaused && !allDead)
            {
                rounds--;
                ticksUsedInfo.SetDataText(rounds + "/" + maxRounds);
                // check if game is finished by failure
                if (rounds == 0 && !finished)
                {
                    levelFails = true;
                }
            }

            if (allDead)
            {
                PauseGame(true);
            }

            if (GameIsPaused)
            {
                yield return new WaitForSeconds(0.1f);
            }
            else
            {
                yield return new WaitForSeconds(tickTime);
            }

            // after waiting we set the failure condition
            // can't check after the wait, because the while statement
            // gets reevaluated and the finished will have changed back
            // to false by then
            LevelFailed = levelFails;

        }
        LevelComplete = true;
    }

    // Update is called once per frame
    void Update()
    {
        triesUsedInfo.SetDataText(Tries + "/" + maxTries);
    }
    public void ResetLevel()
    {
        PauseGame(true);
        ResetTriesAndTicks();
        grid.ResetCells();
        InterruptGame(false);
    }
    private void ResetTriesAndTicks()
    {
        LevelComplete = false;
        LevelFailed = false;
        Tries = maxTries = currentLevel.numTries;
        rounds = maxRounds = currentLevel.numRounds;
        triesUsedInfo.SetDataText(Tries + "/" + maxTries);
        ticksUsedInfo.SetDataText(rounds + "/" + maxRounds);
    }
    public void StartNextLevel()
    {
        PauseGame(true);
        // get next level
        LevelID++;
        LevelID = LevelID % levelPack.Levels.Length;
        currentLevel = levelPack.Levels[LevelID];

        Camera.main.orthographicSize = currentLevel.xSize * 1.2f;

        ResetTriesAndTicks();
        grid.InitialiseCells(maxStateForGame, currentLevel);
        StartCoroutine("Tick");
        InterruptGame(false);
    }

    public void InterruptGame(bool interrupt)
    {
        GameIsInterrupted = interrupt;
        Time.timeScale = GameIsInterrupted ? 0 : 1;
    }

    public void PauseGame(bool pause)
    {
        GameIsPaused = pause;
    }
}
