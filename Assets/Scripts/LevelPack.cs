﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Difficulty
{
    Easy,
    Medium,
    Hard
}

[CreateAssetMenu(fileName = "LevelPack", menuName = "ScriptableObjects/LevelPack")]
public class LevelPack : ScriptableObject
{
    public BaseRule Rule;
    public Level[] Levels;
    public Difficulty Difficulty;
}
