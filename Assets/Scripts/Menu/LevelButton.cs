﻿using TMPro;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class LevelButton : BaseButton
{
    public TextMeshProUGUI text;

    private int id;
    public int ID
    {
        set
        {
            text.text = (value + 1).ToString();
            id = value;
        }
        get
        {
            return id;
        }
    }
}
