﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ToggleButton : MonoBehaviour
{
    public Sprite texture1;
    public Sprite texture2;
    private Image image;
    private void Start()
    {
        image = GetComponent<Image>();
        if(image == null)
        {
            Debug.LogError("Could not find image component on item " + gameObject.name);
        }
    }

    public void ToggleIcon(bool state)
    {
        if (image != null)
        {
            image.sprite = state ? texture1 : texture2;
        }
        
    }
}
