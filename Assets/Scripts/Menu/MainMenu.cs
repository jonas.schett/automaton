﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject buttons;
    public GameObject levelPackSelector;
    public GameObject levelSelector;
    public void PlayGame()
    {
        levelPackSelector.SetActive(true);
        buttons.SetActive(false);
    }

    private void Update()
    {
        // Make sure user is on Android platform
        if (Application.platform == RuntimePlatform.Android)
        {

            // Check if Back was pressed this frame
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                // Go back in menu if back button was pressed
                if(levelPackSelector.activeSelf)
                {
                    levelPackSelector.SetActive(false);
                    buttons.SetActive(true);
                }
                else if(levelSelector.activeSelf)
                {
                    levelSelector.SetActive(false);
                    levelPackSelector.SetActive(true);
                }
                else
                {
                    // if we are on the main screen, quit the application
                    Application.Quit();
                }
            }
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
