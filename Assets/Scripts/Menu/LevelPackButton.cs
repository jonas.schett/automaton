﻿using TMPro;

public class LevelPackButton : BaseButton
{
    public TextMeshProUGUI packName;
    public TextMeshProUGUI numberOfLevels;
    public TextMeshProUGUI ruleName;
    public TextMeshProUGUI difficulty;

    public void Initialise(LevelPack levelPack)
    {
        packName.text = levelPack.name;
        numberOfLevels.text = "Levels: " + levelPack.Levels.Length.ToString();
        ruleName.text = levelPack.Rule.name;
        difficulty.text = levelPack.Difficulty.ToString();
    }
}
