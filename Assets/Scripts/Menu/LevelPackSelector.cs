﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(ScrollRect))]
[RequireComponent(typeof(RectMask2D))]
public class LevelPackSelector : MonoBehaviour
{
    public LevelPack[] levelPacks;
    public LevelPackButton levelPackButtonPrefab;
    public LevelSelector levelSelector;
    public GameObject levelPackSelector;
    public float horizontalButtonScaler;
    public float buttonHeight;
    public float verticalButtonSpacing;

    private GameObject buttonContainer;
    private RectTransform buttonContainerTransform;
    private RectTransform parentTransform;

    private void Start()
    {
        // get initial parentTransform
        parentTransform = GetComponent<RectTransform>();

        // init button container to add buttons to later
        InitialiseButtonContainer();

        // create and add buttons
        InitialiseButtons();

        // create scroll area and add button container transform
        InitialiseScrollArea();
    }
    private void InitialiseScrollArea()
    {
        var scrollArea = GetComponent<ScrollRect>();
        scrollArea.content = buttonContainerTransform;
        scrollArea.horizontal = false;
    }

    private void InitialiseButtonContainer()
    {
        // instantiate buttonContainer itself
        buttonContainer = Instantiate(new GameObject(), transform.position, Quaternion.identity);
        buttonContainer.name = "ButtonContainer";

        // initialise the RectTransform of the buttonContainer
        buttonContainerTransform = buttonContainer.AddComponent<RectTransform>();
        buttonContainerTransform.anchorMin = new Vector2(0, 1);
        buttonContainerTransform.anchorMax = new Vector2(1, 1);
        buttonContainerTransform.pivot = new Vector2(0.5f, 1);
        buttonContainerTransform.sizeDelta = new Vector2(parentTransform.rect.width, levelPacks.Length * (buttonHeight + verticalButtonSpacing));
        buttonContainerTransform.SetParent(transform);
        buttonContainerTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0, buttonContainerTransform.rect.height);

        // adding a vertical layout group to the button so it sorts the buttons correctly
        var verticalLayout = buttonContainer.AddComponent<VerticalLayoutGroup>();
        verticalLayout.childControlWidth = true;
        verticalLayout.childControlHeight = false;
        verticalLayout.padding = new RectOffset(20, 20, 10, 10);
        verticalLayout.spacing = verticalButtonSpacing;
        verticalLayout.childAlignment = TextAnchor.UpperCenter;
        verticalLayout.childScaleHeight = true;
        verticalLayout.childScaleWidth = true;
    }

    private void InitialiseButtons()
    {
        levelPackButtonPrefab.SetSize(horizontalButtonScaler, buttonHeight);
        if (levelPacks != null)
        {
            foreach (var levelPack in levelPacks)
            {
                var button = Instantiate(levelPackButtonPrefab);
                button.Initialise(levelPack);
                button.transform.SetParent(buttonContainerTransform);
                button.GetComponent<Button>().onClick.AddListener(() => ButtonClicked(levelPack));
            }
        }
    }

    void ButtonClicked(LevelPack pack)
    {
        // change view to levelPackView
        levelSelector.SetLevelPack(pack);
        levelSelector.gameObject.SetActive(true);
        levelPackSelector.SetActive(false);
    }
}
