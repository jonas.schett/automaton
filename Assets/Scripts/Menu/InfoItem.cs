﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InfoItem : MonoBehaviour
{
    public string infoTextContent;
    public TextMeshProUGUI infoText;
    public TextMeshProUGUI dataText;
    // Start is called before the first frame update
    void Start()
    {
        infoText.text = infoTextContent;
    }

    public void SetDataText(string content)
    {
        dataText.text = content;
    }

    public void SetDataText(int content)
    {
        SetDataText(content.ToString());
    }
}
