﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelSelector : MonoBehaviour
{
    public LevelPack levelPack;
    public LevelButton buttonPrefab;
    public TextMeshProUGUI nextButtonText;
    public TextMeshProUGUI previousButtonText;
    public int xSize;
    public int ySize;
    public float buttonScaler;

    private int baseLevelID;
    private int levelButtonSize;

    private LevelButton[] buttons;

    private void Start()
    {
        baseLevelID = 0;
    }
    void ButtonClicked(LevelButton button)
    {
        GridManager.LevelID = button.ID;
        GridManager.levelPack = levelPack;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    private void ClearButtons()
    {
        if(buttons != null)
        {
            for(var i = 0; i < buttons.Length; i ++)
            {
                if(buttons[i] != null)
                {
                    Destroy(buttons[i].gameObject);
                }
            }
        }
    }

    public void NextButtonClicked()
    {
        if(!((baseLevelID + (xSize * ySize)) >= levelPack.Levels.Length))
        {
            ClearButtons();
            baseLevelID += (xSize * ySize);
            CreateButtons();
        }
    }
    public void PreviousButtonClicked()
    {
        
        if(!((baseLevelID - (xSize * ySize)) < 0))
        {
            ClearButtons();
            baseLevelID -= (xSize * ySize);
            CreateButtons();
        }
    }
    private void CreateButtons()
    {
        for (int y = 0, i = baseLevelID; y < ySize; y++)
        {
            for (var x = 0; x < xSize && i < levelPack.Levels.Length; x++, i++)
            {
                var button = Instantiate(buttonPrefab);
                button.transform.SetParent(transform);
                button.ID = (i);
                button.transform.localPosition = new Vector2(x * levelButtonSize - xSize * levelButtonSize * 0.5f + levelButtonSize * 0.5f, 
                                                            -y * levelButtonSize + ySize * 0.5f * levelButtonSize - levelButtonSize * 0.5f);

                button.GetComponent<Button>().onClick.AddListener(() => ButtonClicked(button));
                buttons[i%(xSize * ySize)] = button;
            }
        }
    }

    public void SetLevelPack(LevelPack pack)
    {
        ClearButtons();
        levelPack = pack;
        buttons = new LevelButton[xSize * ySize];
        baseLevelID = 0;
        levelButtonSize = buttonPrefab.SetSize(buttonScaler);
        CreateButtons();
    }

}
