﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{

    // Start is called before the first frame update
    public GridManager gameManager;
    public GameObject mainMenu;
    public GameObject onScreenMenu;
    public GameObject finishedLevelInfo;
    public GameObject failedLevelInfo;
    public ToggleButton playButton;

    // Update is called once per frame
    void Update()
    {
        // always ensure the playbutton is up to date with our pause state
        playButton.ToggleIcon(GridManager.GameIsPaused);

        if(GridManager.LevelComplete)
        {
            LevelComplete();
        }
        else if(GridManager.LevelFailed)
        {
            LevelFailed();
        }
    }
    public void ToggleGameState()
    {
        //change current pause state on game
        gameManager.PauseGame(!GridManager.GameIsPaused);
    }

    // Main Menu is the main menu screen,
    // Home Menu is the menu during the game
    public void MainMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
    public void Resume()
    {
        ToggleHomeMenu(false);
    }

    public void HomeMenu()
    {
        ToggleHomeMenu(true);
    }

    public void ToggleHomeMenu(bool toggle)
    {
        // always pause the game no matter what state we are in
        
        gameManager.InterruptGame(toggle);
        gameManager.PauseGame(true);

        // toggle the onscreen menu accordingly
        onScreenMenu.SetActive(!toggle);

        // always ensure these are off
        failedLevelInfo.SetActive(false);
        finishedLevelInfo.SetActive(false);

        // toggle main Menu accordingly
        mainMenu.SetActive(toggle);
    }

    public void LevelComplete()
    {
        gameManager.InterruptGame(true);
        finishedLevelInfo.SetActive(true);
        onScreenMenu.SetActive(false);
    }
    public void LevelFailed()
    {
        gameManager.InterruptGame(true);
        failedLevelInfo.SetActive(true);
        onScreenMenu.SetActive(false);
    }
    public void StartNextLevel()
    {
        gameManager.StartNextLevel();
        finishedLevelInfo.SetActive(false);
        onScreenMenu.SetActive(true);
        gameManager.PauseGame(true);
    }

    // referenced from the UI
    public void ResetLevel()
    {
        HidePopups();
        onScreenMenu.SetActive(true);
        gameManager.ResetLevel();
    }

    private void HidePopups()
    {
        finishedLevelInfo.SetActive(false);
        failedLevelInfo.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
