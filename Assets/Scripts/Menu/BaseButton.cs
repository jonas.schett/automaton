﻿using UnityEngine;

public class BaseButton : MonoBehaviour
{
    public int SetSize(float scaler = 10.0f, float verticalSize = 0)
    {
        scaler = scaler == 0f ? 10.0f : scaler;
        var rectTransform = GetComponent<RectTransform>();

        // make button same size vertically as horizontally if nothing is specified
        verticalSize = verticalSize == 0 ? Screen.width / scaler : verticalSize;
        rectTransform.sizeDelta = new Vector2(Screen.width / scaler, verticalSize);
        return (int)rectTransform.sizeDelta.x;
    }
}
