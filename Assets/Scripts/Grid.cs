﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    [SerializeField]
    private Cell cellPrefab;
    private Cell[,] cells;

    [SerializeField]
    private StateToSpriteMap stateToSpriteMap;
    private Dictionary<State, Tuple<Sprite, Sprite>> stateToSpriteMapDict;

    private Level currentLevel;

    private void Awake()
    {
        // initialise state to material map 
        if (stateToSpriteMap.States.Length == stateToSpriteMap.Sprites.Length && stateToSpriteMap.States.Length == stateToSpriteMap.DesiredSprites.Length)
        {
            stateToSpriteMapDict = new Dictionary<State, Tuple<Sprite, Sprite>>();
            for (int i = 0; i < stateToSpriteMap.States.Length; i++)
            {
                stateToSpriteMapDict.Add(stateToSpriteMap.States[i], Tuple.Create(stateToSpriteMap.Sprites[i], stateToSpriteMap.DesiredSprites[i]));
            }
        }
        else
        {
            Debug.LogError("Must have equal number of states and materials in stateMateiralMap");
        }
    }
    // Function for checking if all cells are dead
    public bool CheckAllDead()
    {
        foreach (var cell in cells)
        {
            if (!(cell.currentState == State.off))
            {
                return false;
            }
        }
        return true;
    }
    // Function for checking if the game is finished
    // which means that all cells have their desired state
    public bool CheckFinished()
    {
        foreach (var cell in cells)
        {
            if (!cell.CheckDesiredState())
            {
                return false;
            }
        }
        return true;
    }
    public void UpdateCells(BaseRule rule)
    {
        foreach (var cell in cells)
        {
            cell.UpdateNextState(rule);
        }
        foreach (var cell in cells)
        {
            cell.ChangeState(true);
        }
    }
    public void ResetCells()
    {
        for (var x = 0; x < currentLevel.xSize; x++)
        {
            for (var y = 0; y < currentLevel.ySize; y++)
            {
                cells[x, y].SetState(currentLevel.states[x + y * currentLevel.xSize], true);
            }
        }
    }
    public void InitialiseCells(State maxStateForGame, Level currentLevel)
    {
        this.currentLevel = currentLevel;

        // destroy current cells
        if (cells != null)
        {
            foreach (var cell in cells)
            {
                Destroy(cell.gameObject);
            }
        }

        int xSize = currentLevel.xSize;
        int ySize = currentLevel.ySize;

        cells = new Cell[xSize, ySize];
        for (int x = 0; x < xSize; x++)
        {
            for (int y = 0; y < ySize; y++)
            {
                var cell = Instantiate(cellPrefab);
                cell.transform.position = new Vector2(x - (xSize - 1) / 2.0f, y - (ySize - 1) / 2.0f);
                cell.transform.SetParent(transform);
                cells[x, y] = cell;
            }
        }

        for (int x = 0; x < xSize; x++)
        {
            for (int y = 0; y < ySize; y++)
            {
                int northCoordinate = y + 1 == ySize ? 0 : y + 1;
                int southCoordinate = y == 0 ? ySize - 1 : y - 1;
                int eastCoordinate = x + 1 == xSize ? 0 : x + 1;
                int westCoordinate = x == 0 ? xSize - 1 : x - 1;
                cells[x, y].neighbours.N = cells[x, northCoordinate];
                cells[x, y].neighbours.NE = cells[eastCoordinate, northCoordinate];
                cells[x, y].neighbours.E = cells[eastCoordinate, y];
                cells[x, y].neighbours.SE = cells[eastCoordinate, southCoordinate];
                cells[x, y].neighbours.S = cells[x, southCoordinate];
                cells[x, y].neighbours.SW = cells[westCoordinate, southCoordinate];
                cells[x, y].neighbours.W = cells[westCoordinate, y];
                cells[x, y].neighbours.NW = cells[westCoordinate, northCoordinate];

                cells[x, y].Initialise(stateToSpriteMapDict, maxStateForGame, currentLevel.coverActive[x + y * xSize], currentLevel.desiredStates[x + y * xSize], currentLevel.states[x + y * xSize]);
            }
        }
    }
}
