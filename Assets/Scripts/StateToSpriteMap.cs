﻿using UnityEngine;

[CreateAssetMenu(fileName = "StateToSpriteMap", menuName = "ScriptableObjects/StateToSpriteMap")]
public class StateToSpriteMap : ScriptableObject
{
    public State[] States;
    public Sprite[] Sprites;
    public Sprite[] DesiredSprites;
}
