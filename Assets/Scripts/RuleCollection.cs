﻿using UnityEngine;

[CreateAssetMenu(fileName = "RuleCollection", menuName = "ScriptableObjects/RuleCollection")]
public class RuleCollection : ScriptableObject
{
    public BaseRule[] rules;
}