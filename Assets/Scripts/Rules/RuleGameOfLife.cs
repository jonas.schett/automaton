﻿using UnityEngine;

[CreateAssetMenu(fileName = "GameOfLifeRule", menuName = "Rules/GameOfLifeRule")]
public class GameOfLifeRule : BaseRule
{
    public override State ApplyRule(Cell[] neighbours, State selfState)
    {
        int neighbourCount = 0;

        foreach (var n in neighbours)
        {
            neighbourCount += n.currentState == State.one ? 1 : 0;
        }
        if (neighbourCount == 3 || (selfState == State.one && neighbourCount == 2))
        {
            return State.one;
        }
        else
        {
            return State.off;
        }
    }
}
