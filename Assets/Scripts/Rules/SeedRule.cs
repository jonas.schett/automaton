﻿using UnityEngine;

[CreateAssetMenu(fileName = "SeedRule", menuName = "Rules/Seed")]
public class SeedRule : BaseRule
{
    public override State ApplyRule(Cell[] neighbours, State selfState)
    {
        if(selfState == State.one)
        {
            return State.off;
        }

        int neighbourLiveCount = 0;

        foreach(var neighbour in neighbours)
        {
            neighbourLiveCount += neighbour.currentState == State.one ? 1 : 0;
        }

        return neighbourLiveCount == 2 ? State.one : State.off;
    }
}
