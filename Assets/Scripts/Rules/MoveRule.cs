﻿using UnityEngine;
public enum MoveDirection
{
    down,
    downLeft,
    left,
    upLeft,
    up,
    upRight,
    right,
    downRight
}
[CreateAssetMenu(fileName = "MoveRule", menuName = "Rules/MoveDirection")]
public class MoveRule : BaseRule
{
    

    public MoveDirection direction;
    public override State ApplyRule(Cell[] neighbours, State selfState)
    {
        return neighbours[(int)direction].currentState;
    }
}
