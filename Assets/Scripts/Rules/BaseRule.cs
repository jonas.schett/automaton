﻿using UnityEngine;

public abstract class BaseRule : ScriptableObject
{
    public string Name;
    public int Tries;
    public int Rounds;
    public State defaultState;
    public abstract State ApplyRule(Cell[] neighbours, State selfState);
}
