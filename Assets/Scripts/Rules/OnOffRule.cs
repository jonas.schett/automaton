﻿using UnityEngine;

[CreateAssetMenu(fileName = "OnOffRule", menuName = "Rules/OnOff")]
public class OnOffRule : BaseRule
{
    public override State ApplyRule(Cell[] neighbours, State selfState)
    {
        if (selfState == State.one)
            return State.off;
        else
            return State.one;
    }
}
