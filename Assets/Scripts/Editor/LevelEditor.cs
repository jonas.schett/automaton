﻿#if (UNITY_EDITOR)

using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class LevelEditor : EditorWindow
{
    private Cell cell;
    private int xSize;
    private int ySize;
    private int tries;
    private int rounds;
    private int ruleIndex;
    private State stateToSet;
    private Cell[,] cells;
    private StateToSpriteMap stateToSpriteMap;
    private Dictionary<State, Tuple<Sprite, Sprite>> stateToSpriteMapDict;
    private RuleCollection ruleCollection;

    private List<Level> levels;
    private int levelPackID;
    private int levelID;

    private bool showCoverSettings = false;
    private bool showStateSettings = true;
    private bool showLevelSettings = true;

    [MenuItem("Window/LevelTools/LevelCreator")]
    public static void ShowWindow()
    {
        GetWindow(typeof(LevelEditor));
    }

    public virtual void OnGUI()
    {
        xSize = EditorGUILayout.IntField("xSize", xSize);
        ySize = EditorGUILayout.IntField("ySize", ySize);
        tries = EditorGUILayout.IntField("tries", tries);
        rounds = EditorGUILayout.IntField("rounds", rounds);
        cell = (Cell)EditorGUILayout.ObjectField("Cell prefab", cell, typeof(Cell), false);
        GUILayout.Space(10);
        stateToSet = (State)EditorGUILayout.EnumPopup("State to set", stateToSet);
        stateToSpriteMap = (StateToSpriteMap)EditorGUILayout.ObjectField("State to material map", stateToSpriteMap, typeof(StateToSpriteMap), false);
        GUILayout.Space(10);
        ruleCollection = (RuleCollection)EditorGUILayout.ObjectField("Rule collection", ruleCollection, typeof(RuleCollection), false);
        ruleIndex = EditorGUILayout.IntField("rule Index", ruleIndex);

        if (xSize > 0 && ySize > 0 && cell != null && stateToSpriteMap != null)
        {
            if(stateToSpriteMapDict == null)
            {
                GenerateStateToMaterialMapDict();
            }
            if(levels == null)
            {
                levels = new List<Level>();
            }

            GUILayout.Space(20);

            if (GUILayout.Button("Generate Map"))
            {
                Clear();
                Generate();
            }
            if (GUILayout.Button("Clear"))
            {
                Clear();
            }
            if (cells != null)
            {
                GUILayout.Space(20);
                showCoverSettings = EditorGUILayout.BeginFoldoutHeaderGroup(showCoverSettings, "Cover settings");
                if(showCoverSettings)
                {
                    if (GUILayout.Button("Turn off covers"))
                    {
                        SwitchCovers(false);
                    }
                    if (GUILayout.Button("Turn on covers"))
                    {
                        SwitchCovers(true);
                    }
                    if (GUILayout.Button("Turn off covers (selected)"))
                    {
                        SwitchCoversSelection(false);
                    }
                    if (GUILayout.Button("Turn on covers (selected)"))
                    {
                        SwitchCoversSelection(true);
                    }
                    
                }
                EditorGUILayout.EndFoldoutHeaderGroup();
                GUILayout.Space(20);
                showStateSettings = EditorGUILayout.BeginFoldoutHeaderGroup(showStateSettings, "State settings");
                if(showStateSettings)
                {
                    if (GUILayout.Button("Set Sate (selected)"))
                    {
                        SetStateSelected();
                    }
                    if (GUILayout.Button("Set desired state (selected)"))
                    {
                        SetDesiredStateSelected();
                    }
                }
                EditorGUILayout.EndFoldoutHeaderGroup();
                GUILayout.Space(20);
                showLevelSettings = EditorGUILayout.BeginFoldoutHeaderGroup(showLevelSettings, "Level settings");
                if(showLevelSettings)
                {
                    levelPackID = EditorGUILayout.IntField("Level Pack", levelPackID);
                    levelID = EditorGUILayout.IntField("Level ID", levelID);
                    if (GUILayout.Button("Add current level to pack"))
                    {
                        AddCurrentLevelToPack();
                    }
                    if (GUILayout.Button("Save levelpack"))
                    {
                        SaveCurrentLevelPack();
                        Debug.Log("LevelPack Saved");
                    }
                }
                EditorGUILayout.EndFoldoutHeaderGroup();
            }
        }
    }

    protected void AddCurrentLevelToPack()
    {
        
        string levelName = "Assets/LevelPacks/LevelPack" + levelPackID.ToString() + "_Level" + levelID.ToString() + ".asset";

        Level level = CreateInstance<Level>();
        level.Initialise(cells, rounds, tries);
        AssetDatabase.CreateAsset(level,levelName);
        AssetDatabase.SaveAssets();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = level;
        levelID++;
        levels.Add(level);
    }

    protected void SaveCurrentLevelPack()
    {
        string packName = "Assets/LevelPacks/LevelPack" + levelPackID.ToString() + ".asset";
        LevelPack levelPack = CreateInstance<LevelPack>();
        levelPack.Levels = levels.ToArray();

        AssetDatabase.CreateAsset(levelPack, packName);
        AssetDatabase.SaveAssets();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = levelPack;
        levelPackID++;
    }

    protected void GenerateStateToMaterialMapDict()
    {
        stateToSpriteMapDict = new Dictionary<State, Tuple<Sprite, Sprite>>();

        if (stateToSpriteMap.States.Length == stateToSpriteMap.Sprites.Length && stateToSpriteMap.States.Length == stateToSpriteMap.DesiredSprites.Length)
        {
            stateToSpriteMapDict = new Dictionary<State, Tuple<Sprite, Sprite>>();
            for (int i = 0; i < stateToSpriteMap.States.Length; i++)
            {
                stateToSpriteMapDict.Add(stateToSpriteMap.States[i], Tuple.Create(stateToSpriteMap.Sprites[i], stateToSpriteMap.Sprites[i]));
            }
        }
        else
        {
            Debug.LogError("Must have equal number of states and materials in stateMateiralMap");
        }
    }

    protected List<Cell> GetSelectedCells()
    {
        List<Cell> returnList = new List<Cell>();

        foreach (GameObject g in Selection.gameObjects)
        {
            Cell cell = g.GetComponent<Cell>();
            if (cell != null)
            {
                returnList.Add(cell);
            }
        }

        return returnList;
    }

    protected void SetDesiredStateSelected()
    {
        GenerateStateToMaterialMapDict();
        var list = GetSelectedCells();
        foreach(var cell in list)
        {
            cell.SetDesiredStateEditor(stateToSet);
        }
    }

    protected void SetStateSelected()
    {
        GenerateStateToMaterialMapDict();
        var list = GetSelectedCells();
        foreach(var cell in list)
        {
            cell.SetState(stateToSet);
        }
    }

    protected void SwitchCoversSelection(bool onOff)
    {
        var list = GetSelectedCells();
        foreach(var cell in list)
        {
            cell.SetCoverActive(onOff);
        }
    }

    protected void SwitchCovers(bool onOrOff)
    {
        foreach(var cell in cells)
        {
            cell.SetCoverActive(onOrOff);
        }
    }

    protected void Clear()
    {
        if (cells != null)
        {
            foreach (var cell in cells)
            {
                DestroyImmediate(cell.gameObject);
            }
        }
        cells = null;
    }

    protected void Generate()
    {
        cells = new Cell[xSize, ySize];
        for(int x = 0; x < xSize; x++)
        {
            for(int y = 0; y < ySize; y++)
            {
                cells[x, y] = Instantiate(cell);
                cells[x, y].transform.position = new Vector3(x - (xSize - 1) / 2.0f, 0, y - (ySize - 1) / 2.0f);
                cells[x, y].Initialise(stateToSpriteMapDict, State.one, false);
                cells[x, y].SetStateToMaterialMap(stateToSpriteMapDict);
            }
        }
    }

    public static string GetMapDirectory()
    {
        string directory = Path.Combine(Application.persistentDataPath, "MapData");
        if (!Directory.Exists(directory))
        {
            Directory.CreateDirectory(directory);
        }
        return directory;
    }
}

#endif
