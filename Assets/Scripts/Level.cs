﻿using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "ScriptableObjects/Level")]
public class Level : ScriptableObject
{
    public int xSize;
    public int ySize;

    public int numRounds;
    public int numTries;

    public State[] states;
    public State[] desiredStates;
    public bool[] coverActive;

    public void Initialise(Cell[,] cells, int rounds, int tries)
    {
        numRounds = rounds;
        numTries = tries;

        xSize = cells.GetLength(0);
        ySize = cells.GetLength(1);
        states = new State[xSize * ySize];
        desiredStates = new State[xSize * ySize];
        coverActive = new bool[xSize * ySize];

        for (var x = 0; x < xSize; x++)
        {
            for (var y = 0; y < ySize; y++)
            {
                states[x + y * xSize] = cells[x, y].currentState;
                desiredStates[x + y * xSize] = cells[x, y].desiredState;
                coverActive[x + y * xSize] = cells[x, y].CoverActive;
            }
        }
    }
}
